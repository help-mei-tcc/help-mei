package com.helpmeiserver.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProduto;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_estoque")
    private Estoque estoque;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_movimentacao")
    private Movimentacao movimentacao;

    private String nome;
    private String fornecedor;
    private LocalDate dataInclusao;
    private BigDecimal valorVenda;
    private BigDecimal valorProducao;
}

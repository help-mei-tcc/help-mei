package com.helpmeiserver.model;

import com.helpmeiserver.model.enums.AreaAtuacao;
import lombok.*;

import jakarta.persistence.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Mei extends Usuario {

    @Enumerated(EnumType.STRING)
    private AreaAtuacao areaAtuacao;

    @OneToMany(mappedBy = "mei", cascade = CascadeType.ALL)
    private List<Relatorio> relatorios;

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
    protected List<NotaFiscal> notasFiscais;

    @Column(unique = true)
    private String cnpj;
    private String nome;
    private String nomeEmpresa;
}
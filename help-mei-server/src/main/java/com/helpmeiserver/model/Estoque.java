package com.helpmeiserver.model;

import lombok.Data;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
public class Estoque {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEstoque;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    @OneToMany(mappedBy = "estoque", cascade = CascadeType.ALL)
    private List<Produto> produtos;

    private BigDecimal valor;
}

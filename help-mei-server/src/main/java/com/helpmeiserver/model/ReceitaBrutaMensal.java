package com.helpmeiserver.model;

import com.helpmeiserver.model.enums.AreaAtuacao;
import lombok.Data;

@Data
public class ReceitaBrutaMensal {
    private AreaAtuacao categoria;
    private double mercadoriasComEmissaoNf;
    private double mercadoriasSemEmissaoNf;
    private double totalMercadorias;

    public ReceitaBrutaMensal() {
    }

    public ReceitaBrutaMensal(AreaAtuacao categoria, double mercadoriasComEmissaoNf, double mercadoriasSemEmissaoNf, double totalMercadorias) {
        this.categoria = categoria;
        this.mercadoriasComEmissaoNf = mercadoriasComEmissaoNf;
        this.mercadoriasSemEmissaoNf = mercadoriasSemEmissaoNf;
        this.totalMercadorias = totalMercadorias;
    }
}

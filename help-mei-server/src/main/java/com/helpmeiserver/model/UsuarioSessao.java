package com.helpmeiserver.model;

import lombok.Data;

import jakarta.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class UsuarioSessao {

    public static Usuario usuarioLogado;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSessao;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    private LocalDate criadaEm;

    public UsuarioSessao() {
    }

    public UsuarioSessao(Usuario usuario) {
        this.usuario = usuario;
        this.criadaEm = LocalDate.now();

        this.usuarioLogado = usuario;
    }
}
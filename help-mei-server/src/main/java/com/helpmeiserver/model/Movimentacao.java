package com.helpmeiserver.model;

import com.helpmeiserver.model.enums.CategoriaMovimentacao;
import com.helpmeiserver.model.enums.TipoMovimentacao;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Movimentacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMovimentacao;

    @OneToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pedido")
    private Pedido pedido;

    @OneToMany(mappedBy = "movimentacao", cascade = CascadeType.ALL)
    private List<Produto> produtos;

    @Enumerated(EnumType.STRING)
    private CategoriaMovimentacao categoria;

    @Enumerated(EnumType.STRING)
    private TipoMovimentacao tipoMovimentacao;

    private String descricao;
    private LocalDate dataInsercao;
    private BigDecimal valor;
}

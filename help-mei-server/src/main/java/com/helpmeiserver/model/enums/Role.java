package com.helpmeiserver.model.enums;

public enum Role {
    USER,
    ADMIN
}

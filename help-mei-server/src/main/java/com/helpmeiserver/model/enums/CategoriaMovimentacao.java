package com.helpmeiserver.model.enums;

public enum CategoriaMovimentacao {
    AGUA_E_LUZ("Água e luz"),
    ALUGUEL("Aluguel"),
    APLICACOES_FINANCEIRAS("Aplicacoes financeiras"),
    COMBUSTIVEL("Combustível"),
    EMPRESTIMO("Empréstimo"),
    FORNECEDORES("Fornecedores"),
    FRETE("Frete"),
    IMPOSTOS_E_TAXAS("Impostos e taxas"),
    INVESTIMENTO_EXTERNO("Investimento externo"),
    INVESTIMENTO_INTERNO("Investimento interno"),
    OUTROS("Outros"),
    PRESENTE("Presente"),
    PRO_LABORE("Pro labore"),
    SALARIO("Salário"),
    SERVICOS_CONTRATADOS("Serviços contratados"),
    SERVICOS_PRESTADOS("Servicos prestados"),
    TELEFONE_E_INTERNET("Telefone e internet"),
    VENDAS("Vendas");

    private final String valor;

    CategoriaMovimentacao(String valor) { this.valor = valor; }

    public String getValor() {
        return valor;
    }
}

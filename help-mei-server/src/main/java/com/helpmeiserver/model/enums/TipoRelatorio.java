package com.helpmeiserver.model.enums;

public enum TipoRelatorio {
    DAS_MEI("das-mei"),
    DASN_SIMEI("dasn-simei");

    private final String valor;

    TipoRelatorio(String valor) { this.valor = valor; }

}
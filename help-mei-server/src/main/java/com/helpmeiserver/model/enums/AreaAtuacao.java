package com.helpmeiserver.model.enums;

public enum AreaAtuacao {
    SERVICOS("servicos"),
    COMERCIO("comercio"),
    INDUSTRIA("industria");

    private final String valor;

    AreaAtuacao(String valor) { this.valor = valor; }

    }
package com.helpmeiserver.model.enums;

public enum TipoMovimentacao {
    ENTRADA("entrada"),
    SAIDA("saída");

    private final String valor;

    TipoMovimentacao(String valor){
        this.valor = valor;
    }
}

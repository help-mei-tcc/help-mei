package com.helpmeiserver.model;

import com.helpmeiserver.model.enums.Role;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long idUsuario;

    @Enumerated(EnumType.STRING)
    protected Role role;

    @Column(unique = true)
    protected String email;
    protected String senha;

}
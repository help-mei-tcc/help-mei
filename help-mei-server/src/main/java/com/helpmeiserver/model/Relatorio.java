package com.helpmeiserver.model;

import com.helpmeiserver.model.enums.TipoRelatorio;
import lombok.Data;

import jakarta.persistence.*;
import java.io.File;

@Data
@Entity
public class Relatorio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRelatorio;

    @Enumerated(EnumType.STRING)
    private TipoRelatorio tipoRelatorio;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario")
    private Mei mei;

    private File arquivo;

    public Relatorio() { }

    public Relatorio(Long idRelatorio, TipoRelatorio tipoRelatorio, File arquivo) {
        this.idRelatorio = idRelatorio;
        this.tipoRelatorio = tipoRelatorio;
        this.arquivo = arquivo;
    }

}
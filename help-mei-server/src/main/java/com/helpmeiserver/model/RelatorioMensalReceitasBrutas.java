package com.helpmeiserver.model;

import lombok.Data;

import java.util.List;

@Data
public class RelatorioMensalReceitasBrutas {
    private String cnpj;
    private String empreendedorIndividual;
    private String periodoApuracao;
    private List<ReceitaBrutaMensal> categoriasReceita;
    private double totalReceitasBrutasNoMes;

    public RelatorioMensalReceitasBrutas() {
    }

    public RelatorioMensalReceitasBrutas(String cnpj, String empreendedorIndividual, String periodoApuracao, List<ReceitaBrutaMensal> categoriasReceita, double totalReceitasBrutasNoMes) {
        this.cnpj = cnpj;
        this.empreendedorIndividual = empreendedorIndividual;
        this.periodoApuracao = periodoApuracao;
        this.categoriasReceita = categoriasReceita;
        this.totalReceitasBrutasNoMes = totalReceitasBrutasNoMes;
    }
}

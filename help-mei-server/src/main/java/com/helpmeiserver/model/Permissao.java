package com.helpmeiserver.model;


import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Permissao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPermissao;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "permissao_usuario",
            joinColumns = @JoinColumn(name = "id_permissao"),
            inverseJoinColumns = @JoinColumn(name = "id_usuario"))
    private List<Usuario> usuarios;

    private String titulo;

    private String descricao;

    public Permissao() { }

    public Permissao(Long idPermissao, String titulo, String descricao) {
        this.idPermissao = idPermissao;
        this.titulo = titulo;
        this.descricao = descricao;
    }

}
package com.helpmeiserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpMeiServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelpMeiServerApplication.class, args);
	}

}

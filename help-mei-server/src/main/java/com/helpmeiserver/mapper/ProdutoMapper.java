package com.helpmeiserver.mapper;

import com.helpmeiserver.controller.request.ProdutoRequest;
import com.helpmeiserver.model.Produto;

import java.time.LocalDate;

public class ProdutoMapper {

    public static Produto toEntity(ProdutoRequest target) {
        Produto entity = new Produto();

        entity.setNome(target.getNome());
        entity.setFornecedor(target.getFornecedor());
        entity.setValorProducao(target.getValorProducao());
        entity.setValorVenda(target.getValorVenda());
        entity.setDataInclusao(LocalDate.now());

        return entity;
    }
}

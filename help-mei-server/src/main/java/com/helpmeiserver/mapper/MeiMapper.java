package com.helpmeiserver.mapper;

import com.helpmeiserver.controller.request.MeiRequest;
import com.helpmeiserver.controller.response.MeiResponse;
import com.helpmeiserver.model.Mei;
import org.modelmapper.ModelMapper;

public class MeiMapper {

    private static ModelMapper mapper = new ModelMapper();

    public static Mei toEntity(MeiRequest target) {
        Mei usuarioMei = Mei.builder().build();

        usuarioMei.setNome(target.getNome());
        usuarioMei.setEmail(target.getEmail());
        usuarioMei.setCnpj(target.getCnpj());
        usuarioMei.setSenha(target.getSenha());
        usuarioMei.setNomeEmpresa(target.getNomeEmpresa());
        usuarioMei.setAreaAtuacao(target.getAreaAtuacao());

        return usuarioMei;
    }

    public static MeiResponse toResponse(Mei entity) {
        MeiResponse response = new MeiResponse();

        response.setIdMei(entity.getIdUsuario());
        response.setNome(entity.getNome());
        response.setEmail(entity.getEmail());
        response.setCnpj(entity.getCnpj());
        response.setSenha(entity.getSenha());
        response.setNomeEmpresa(entity.getNomeEmpresa());
        response.setAreaAtuacao(entity.getAreaAtuacao());

        return response;
    }

}

package com.helpmeiserver.mapper;

import com.helpmeiserver.controller.request.LembreteRequest;
import com.helpmeiserver.model.Lembrete;

public class LembreteMapper {

    public static Lembrete toEntity(LembreteRequest target) {
        Lembrete entidade = new Lembrete();

        entidade.setData(target.getData());
        entidade.setMensagem(target.getMensagem());
        entidade.setTitulo(target.getTitulo());

        return entidade;
    }
}

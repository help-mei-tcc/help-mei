package com.helpmeiserver.mapper;

import com.helpmeiserver.controller.request.MovimentacaoRequest;
import com.helpmeiserver.controller.response.MovimentacaoResponse;
import com.helpmeiserver.model.Movimentacao;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MovimentacaoMapper {

    private static ModelMapper mapper = new ModelMapper();

    public static Movimentacao toEntity(MovimentacaoRequest target) {
        Movimentacao entidade = new Movimentacao();

        entidade.setValor(target.getValor());
        entidade.setDataInsercao(LocalDate.now());
        entidade.setDescricao(target.getDescricao());
        entidade.setCategoria(target.getCategoria());
        entidade.setTipoMovimentacao(target.getTipoMovimentacao());

        return entidade;
    }

    public static MovimentacaoResponse toResponse(Movimentacao entity) {
        MovimentacaoResponse response = new MovimentacaoResponse();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        response.setValor(entity.getValor());
        response.setPedido(entity.getPedido());
        response.setDescricao(entity.getDescricao());
        response.setCategoria(entity.getCategoria().getValor());
        response.setIdMovimentacao(entity.getIdMovimentacao());
        response.setTipoMovimentacao(entity.getTipoMovimentacao());
        response.setDataInsercao(entity.getDataInsercao().format(formatter));

        return response;
    }
}

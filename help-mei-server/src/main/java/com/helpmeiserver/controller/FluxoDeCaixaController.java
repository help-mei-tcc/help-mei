package com.helpmeiserver.controller;

import com.helpmeiserver.controller.request.MovimentacaoRequest;
import com.helpmeiserver.controller.response.MovimentacaoResponse;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.service.FluxoDeCaixaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/fluxo-de-caixa")
public class FluxoDeCaixaController {

    private final FluxoDeCaixaService service;

    @PostMapping("/cadastrar/movimentacao")
    public ResponseEntity<ResponseMessage> cadastrar(@RequestBody MovimentacaoRequest request) {
        try {
            ResponseMessage response = service.cadastrar(request);

            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @GetMapping("/buscar/movimentacoes/usuario/{idUsuario}")
    public ResponseEntity<List<MovimentacaoResponse>> buscarPorUsuario(@PathVariable Long idUsuario) {
        try {
            return ResponseEntity.ok(service.buscarPorUsuario(idUsuario));
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @GetMapping("movimentacao/{idUsuario}/{idMovimentacao}")
    public MovimentacaoResponse buscarMovimentacao(@PathVariable Long idUsuario , @PathVariable Long idMovimentacao) {
        try {
            return service.buscarPorId(idUsuario, idMovimentacao);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @PutMapping("/editar/movimentacao/{idUsuario}/{idMovimentacao}")
    public ResponseEntity<ResponseMessage> editarMovimentacao(@PathVariable Long idUsuario, @PathVariable Long idMovimentacao, @RequestBody MovimentacaoRequest request) {
        try {
            ResponseMessage response = service.editar(idUsuario, idMovimentacao, request);

            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @DeleteMapping("/deletar/movimentacao/{idMovimentacao}/usuario/{idUsuario}")
    public ResponseEntity<ResponseMessage> excluirMovimentacao(@PathVariable Long idUsuario, @PathVariable Long idMovimentacao) {
        try {
            ResponseMessage response = service.excluir(idUsuario, idMovimentacao);

            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}

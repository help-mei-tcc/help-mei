package com.helpmeiserver.controller;

import com.helpmeiserver.controller.request.ProdutoRequest;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.model.Produto;
import com.helpmeiserver.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path= "/produto")
public class ProdutoController {

    @Autowired
    ProdutoService service;

    @PostMapping("{idMei}")
    public ResponseEntity<Object> Cadastrar(@RequestBody ProdutoRequest request, @PathVariable Long idMei) {
        try {
            ResponseMessage response = service.cadastrar(request, idMei);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PutMapping("{idMei}")
    public ResponseEntity<Object> Editar(@RequestBody ProdutoRequest request, @PathVariable Long idMei, @PathVariable Long idProduto) {
        try {
            ResponseMessage response = service.editar(request, idMei, idProduto);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @GetMapping("{idMei}")
    public ResponseEntity<List<Produto>> BuscarTodosPorMei(@PathVariable Long idMei) {
        try {
            List<Produto> response = service.buscarTodosPorMei(idMei);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("{idMei}/{idProduto}")
    public ResponseEntity<Produto> BuscarPorId(@PathVariable Long idMei, @PathVariable Long idProduto) {
        try {
            Produto response = service.buscarPorId(idMei, idProduto);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @DeleteMapping("{idMei}/{idProduto}")
    public ResponseEntity<Object> Deletar(@PathVariable Long idMei, @PathVariable Long idProduto) {
        try {
            ResponseMessage response = service.deletar(idMei, idProduto);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }
}

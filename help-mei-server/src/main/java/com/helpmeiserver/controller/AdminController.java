package com.helpmeiserver.controller;

import com.helpmeiserver.controller.request.*;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.model.Admin;
import com.helpmeiserver.model.Mei;
import com.helpmeiserver.repository.AdminRepository;
import com.helpmeiserver.service.AdminService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private AdminService service;

    @Autowired
    private AdminRepository repo;

    @PostMapping("cadastrar-mei/{idResponsavel}")
    public ResponseEntity<Object> cadastrarMei(@RequestBody MeiRequest request, @PathVariable Long idResponsavel) {
        try {
            ResponseMessage response = service.cadastrarMei(request, idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PutMapping("editar-mei/{idMei}/{idResponsavel}")
    public ResponseEntity<Object> editarMei(@RequestBody MeiRequest request, @PathVariable Long idMei, @PathVariable Long idResponsavel) {
        try {
            ResponseMessage response = service.editarMei(request, idMei, idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @GetMapping("buscar-mei/{idResponsavel}")
    public ResponseEntity<List<Mei>> buscarTodosMei(@PathVariable Long idResponsavel) {
        try {
            List<Mei> response = service.buscarTodosMei(idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @DeleteMapping("deletar-mei/{idMei}/{idResponsavel}")
    public ResponseEntity<Object> deletarMei(@PathVariable Long idMei, @PathVariable Long idResponsavel) {
        try {
            ResponseMessage response = service.deletarMei(idMei, idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PostMapping("criar-admin")
    public ResponseEntity<Object> apagaDepois(@RequestBody Admin request) {
        repo.save(request);

        return ResponseEntity.ok("show");
    }
}

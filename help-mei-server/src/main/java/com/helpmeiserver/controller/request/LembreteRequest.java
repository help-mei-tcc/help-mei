package com.helpmeiserver.controller.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class LembreteRequest {

    private String titulo;

    private String mensagem;

    private LocalDate data;

    public LembreteRequest() {
    }

    public LembreteRequest(String titulo, String mensagem, LocalDate data) {
        this.titulo = titulo;
        this.mensagem = mensagem;
        this.data = data;
    }
}

package com.helpmeiserver.controller.request;

import com.helpmeiserver.model.enums.AreaAtuacao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MeiRequest {
    private String nome;
    private String email;
    private String cnpj;
    private String senha;
    private String nomeEmpresa;
    private AreaAtuacao areaAtuacao;

    public MeiRequest() {
    }

    public MeiRequest(String nome, String email, String cnpj, String senha, String nomeEmpresa, AreaAtuacao areaAtuacao) {
        this.nome = nome;
        this.email = email;
        this.cnpj = cnpj;
        this.senha = senha;
        this.nomeEmpresa = nomeEmpresa;
        this.areaAtuacao = areaAtuacao;
    }
}

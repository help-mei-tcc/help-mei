package com.helpmeiserver.controller.request;

import com.helpmeiserver.model.enums.CategoriaMovimentacao;
import com.helpmeiserver.model.enums.TipoMovimentacao;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class MovimentacaoRequest {
    private String descricao;
    private Long idUsuario;
    private Long idPedido;
    private CategoriaMovimentacao categoria;
    private TipoMovimentacao tipoMovimentacao;

    private BigDecimal valor;

    private boolean novoPedido;

    private List<Long> listaProdutos;

    public MovimentacaoRequest() {

    }

    public MovimentacaoRequest(String descricao, Long idUsuario, Long idPedido, CategoriaMovimentacao categoria, TipoMovimentacao tipoMovimentacao, BigDecimal valor, boolean novoPedido, List<Long> listaProdutos) {
        this.descricao = descricao;
        this.idUsuario = idUsuario;
        this.idPedido = idPedido;
        this.categoria = categoria;
        this.tipoMovimentacao = tipoMovimentacao;
        this.valor = valor;
        this.novoPedido = novoPedido;
        this.listaProdutos = listaProdutos;
    }
}

package com.helpmeiserver.controller.request;

import com.helpmeiserver.model.enums.AreaAtuacao;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CadastroRequest {

    private AutenticacaoRequest credenciais;
    private AreaAtuacao areaAtuacao;
    private String nome;
    private String nomeEmpresa;
    private String cnpj;
}

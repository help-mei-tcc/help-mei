package com.helpmeiserver.controller.request;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class ProdutoRequest {
    private String nome;
    private String fornecedor;
    private LocalDate dataInclusao;
    private BigDecimal valorVenda;
    private BigDecimal valorProducao;
}

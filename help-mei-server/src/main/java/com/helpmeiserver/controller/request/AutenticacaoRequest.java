package com.helpmeiserver.controller.request;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AutenticacaoRequest {

    private String email;
    private String senha;

}

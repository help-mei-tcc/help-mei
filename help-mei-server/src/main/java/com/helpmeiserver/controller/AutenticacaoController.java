package com.helpmeiserver.controller;

import com.helpmeiserver.controller.request.AutenticacaoRequest;
import com.helpmeiserver.controller.request.CadastroRequest;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.model.Admin;
import com.helpmeiserver.repository.AdminRepository;
import com.helpmeiserver.service.AutenticacaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/autenticacao")
public class AutenticacaoController {

    private final AutenticacaoService service;
    private final AdminRepository adminRepository;

    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody AutenticacaoRequest request) {
        try {
            return ResponseEntity
                    .ok(service.efetuarLogin(request));

        } catch (RuntimeException exception) {
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseMessage(exception.getMessage()));
        }
    }

    @CrossOrigin
    @PostMapping("/cadastro")
    public ResponseEntity<Object> registrar(@RequestBody CadastroRequest request) {
        try {
            return ResponseEntity.ok(service.efetuarCadastro(request));

        } catch (Exception exception) {
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseMessage(exception.getMessage()));
        }
    }
}

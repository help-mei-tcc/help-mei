package com.helpmeiserver.controller.response;

import com.helpmeiserver.model.Pedido;
import com.helpmeiserver.model.enums.CategoriaMovimentacao;
import com.helpmeiserver.model.enums.TipoMovimentacao;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovimentacaoResponse {

    private Long idMovimentacao;
    private Pedido pedido;
    private BigDecimal valor;
    private String descricao;
    private String dataInsercao;
    private String categoria;
    private TipoMovimentacao tipoMovimentacao;
}

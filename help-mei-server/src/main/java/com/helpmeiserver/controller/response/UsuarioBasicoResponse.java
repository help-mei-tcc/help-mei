package com.helpmeiserver.controller.response;

import com.helpmeiserver.model.enums.AreaAtuacao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioBasicoResponse {

    private Long idMei;
    private String nome;
    private String email;
    private String cnpj;
    private String senha;
    private String nomeEmpresa;
    private AreaAtuacao areaAtuacao;
}

package com.helpmeiserver.controller.response;

import com.helpmeiserver.model.enums.AreaAtuacao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MeiResponse {

    private Long idMei;
    private String nome;
    private String email;
    private String cnpj;
    private String senha;
    private String nomeEmpresa;
    private AreaAtuacao areaAtuacao;

    public MeiResponse() {}

}

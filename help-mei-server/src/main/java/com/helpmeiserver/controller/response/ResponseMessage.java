package com.helpmeiserver.controller.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseMessage {
    public String message;

    public ResponseMessage(String message){
        this.message = message;
    }

    public static ResponseMessage retornar(String message){
        return new ResponseMessage(message);
    }
}

package com.helpmeiserver.controller;

import com.helpmeiserver.controller.request.MeiRequest;
import com.helpmeiserver.controller.response.MeiResponse;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.service.MeiService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/usuario")
public class MeiController {

    private final MeiService service;

    @GetMapping("{idUsuario}")
    public MeiResponse buscar(@PathVariable Long idUsuario) {
        try {
            return service.buscarPorId(idUsuario);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @PutMapping("{idUsuario}")
    public ResponseEntity<ResponseMessage> editar(@PathVariable Long idUsuario, @RequestBody MeiRequest request) {
        try {
            ResponseMessage response = service.editar(idUsuario, request);

            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @DeleteMapping("{idUsuario}")
    public ResponseEntity<ResponseMessage> excluir(@PathVariable Long idUsuario) {
        try {
            ResponseMessage response = service.excluir(idUsuario);

            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @GetMapping("relatorio-mensal-receitas-brutas/{idUsuario}")
    public ResponseMessage gerarRelatorioMensalDeReceitasBrutas(@PathVariable Long idUsuario) {
        try {
            return service.gerarRelatorioMensalDeReceitasBrutas(idUsuario);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}

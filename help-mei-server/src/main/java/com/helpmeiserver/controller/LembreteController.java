package com.helpmeiserver.controller;

import com.helpmeiserver.controller.request.LembreteRequest;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.model.Lembrete;
import com.helpmeiserver.service.LembreteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/lembrete")
public class LembreteController {

    @Autowired
    private LembreteService service;

    @PostMapping("/cadastrar/{idResponsavel}")
    public ResponseEntity<Object> cadastrarLembrete(@RequestBody LembreteRequest request, @PathVariable Long idResponsavel) {
        try {
            ResponseMessage response = service.cadastrarLembrete(request, idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }


    @PutMapping("/editar/{idLembrete}/{idResponsavel}")
    public ResponseEntity<Object> editarLembrete(@RequestBody LembreteRequest request, @PathVariable Long idLembrete, @PathVariable Long idResponsavel) {
        try {
            ResponseMessage response = service.editarLembrete(request, idLembrete, idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }


    @GetMapping("buscar/{idResponsavel}")
    public ResponseEntity<List<Lembrete>> buscarTodosLembretes(@PathVariable Long idResponsavel) {
        try {
            List<Lembrete> response = service.buscarTodosLembretes(idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("/buscar")
    public ResponseEntity<List<Lembrete>> buscarLembrete() {
        try {
            return ResponseEntity.ok(service.buscarLembretes());
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }


    @DeleteMapping("/deletar/{idLembrete}/{idResponsavel}")
    public ResponseEntity<Object> deletarLembrete(@PathVariable Long idLembrete, @PathVariable Long idResponsavel) {
        try {
            ResponseMessage response = service.deletarLembrete(idLembrete, idResponsavel);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(null);
        }
    }

}

package com.helpmeiserver.constants;

public class Constants {

    public static final String PERMISSAO_ADMIN = "Admin";

    public static final Long DIAS_ANTERIORES = 7L;

}
package com.helpmeiserver.service;

import com.helpmeiserver.controller.request.MovimentacaoRequest;
import com.helpmeiserver.controller.response.MovimentacaoResponse;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.mapper.MovimentacaoMapper;
import com.helpmeiserver.model.*;
import com.helpmeiserver.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FluxoDeCaixaService {

    private final MovimentacaoRepository repository;
    private final MeiRepository meiRepository;
    private final PedidoRepository pedidoRepository;
    private final EstoqueRepository estoqueRepository;
    private final ProdutoRepository produtoRepository;
    private final NotaFiscalRepository notaFiscalRepository;
    private final MovimentacaoRepository movimentacaoRepository;

    public ResponseMessage cadastrar(MovimentacaoRequest request) {
        Optional<Mei> usuario = meiRepository.findByIdUsuario(request.getIdUsuario());

        if (!usuario.isPresent())
            throw new RuntimeException("Não foi possível cadastrar a movimentação.");

        Mei usuarioMei = usuario.get();

        Estoque estoque = estoqueRepository.findByUsuario(usuarioMei);
        Movimentacao movimentacao = MovimentacaoMapper.toEntity(request);
        NotaFiscal notaFiscal = notaFiscalRepository.findByUsuario(usuarioMei);

        atribuiProdutos(movimentacao, request.getListaProdutos());

        boolean gerouNotaFiscal = notaFiscal != null;

        if (request.isNovoPedido()) {
            List<Movimentacao> movimentacoes = new ArrayList<>();
            movimentacoes.add(movimentacao);

            Pedido novoPedido = Pedido.builder()
                    .movimentacoes(movimentacoes)
                    .usuario(usuarioMei)
                    .estoque(estoque)
                    .gerouNotaFiscal(gerouNotaFiscal)
                    .build();

            pedidoRepository.save(novoPedido);
            movimentacao.setPedido(novoPedido);
        }

        movimentacao.setUsuario(usuarioMei);
        repository.save(movimentacao);

        return new ResponseMessage("Movimentação efetuada com sucesso.");
    }

    private void atribuiProdutos(Movimentacao movimentacao, List<Long> listaProdutos) {
        List<Produto> produtos = listaProdutos.stream()
                .map(idProduto -> {

                    Produto produto = produtoRepository.findByIdProduto(idProduto);
                    produto.setMovimentacao(movimentacao);

                    return produto;
                }).toList();

        movimentacao.setProdutos(produtos);
    }

    public List<MovimentacaoResponse> buscarPorUsuario(Long idUsuario) {
        Optional<Mei> usuario = meiRepository.findByIdUsuario(idUsuario);

        if (!usuario.isPresent())
            throw new RuntimeException("Usuário não encontrado.");

        List<Movimentacao> movimentacoes = movimentacaoRepository.findByUsuarioIdUsuario(idUsuario);
        List<MovimentacaoResponse> response = movimentacoes.stream().map(movimentacao -> MovimentacaoMapper.toResponse(movimentacao)).toList();

        return response;
    }

    public MovimentacaoResponse buscarPorId(Long idUsuario, Long idMovimentacao) {
        Optional<Mei> usuario = meiRepository.findByIdUsuario(idUsuario);

        if (!usuario.isPresent())
            throw new RuntimeException("Não foi possível encontrar o usuário correspondente.");

        Optional<Movimentacao> movimentacao = movimentacaoRepository.findByIdMovimentacaoAndUsuarioIdUsuario(idMovimentacao, usuario.get().getIdUsuario());

        if (!movimentacao.isPresent())
            throw new RuntimeException("A movimentação não corresponde a este usuário.");

        MovimentacaoResponse response = MovimentacaoMapper.toResponse(movimentacao.get());

        return response;
    }

    public ResponseMessage editar(Long idUsuario, Long idMovimentacao, MovimentacaoRequest request) {
        Optional<Mei> usuario = meiRepository.findByIdUsuario(idUsuario);

        if (!usuario.isPresent())
            throw new RuntimeException("Não foi possível encontrar o usuário correspondente.");

        Optional<Movimentacao> movimentacaoOptional = movimentacaoRepository.findByIdMovimentacaoAndUsuarioIdUsuario(idMovimentacao, usuario.get().getIdUsuario());

        if (!movimentacaoOptional.isPresent())
            throw new RuntimeException("A movimentação não corresponde a este usuário.");

        Movimentacao movimentacao = movimentacaoOptional.get();

        movimentacao.setValor(request.getValor());
        movimentacao.setTipoMovimentacao(request.getTipoMovimentacao());
        movimentacao.setCategoria(request.getCategoria());
        movimentacao.setDescricao(request.getDescricao());
        movimentacao.setDataInsercao(LocalDate.now());

        repository.save(movimentacao);

        return new ResponseMessage("Movimentação editada com sucesso.");
    }

    public ResponseMessage excluir(Long idUsuario, Long idMovimentacao) {

        Optional<Mei> usuario = meiRepository.findByIdUsuario(idUsuario);

        if (!usuario.isPresent())
            throw new RuntimeException("Não foi possível encontrar o usuário correspondente.");

        Optional<Movimentacao> movimentacaoOptional = movimentacaoRepository.findByIdMovimentacaoAndUsuarioIdUsuario(idMovimentacao, usuario.get().getIdUsuario());

        if (!movimentacaoOptional.isPresent())
            throw new RuntimeException("A movimentação não corresponde a este usuário.");

        Movimentacao movimentacao = movimentacaoOptional.get();
        repository.delete(movimentacao);

        return new ResponseMessage("Movimentação excluida com sucesso.");
    }
}

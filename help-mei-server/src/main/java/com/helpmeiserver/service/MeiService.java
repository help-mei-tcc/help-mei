package com.helpmeiserver.service;

import com.helpmeiserver.controller.request.MeiRequest;
import com.helpmeiserver.controller.response.MeiResponse;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.mapper.MeiMapper;
import com.helpmeiserver.model.*;
import com.helpmeiserver.model.enums.AreaAtuacao;
import com.helpmeiserver.repository.EstoqueRepository;
import com.helpmeiserver.repository.MeiRepository;
import com.helpmeiserver.repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class MeiService<T> {

    @Autowired
    private MeiRepository repository;

    @Autowired
    private EstoqueRepository estoqueRepository;

    @Autowired
    private MovimentacaoRepository movimentacaoRepository;

    public MeiResponse buscarPorId(Long idUsuario) {
        Mei mei = (Mei) BuscaPorId(idUsuario, repository);

        if (mei == null)
            throw new RuntimeException("Usuário não encontrado.");

        MeiResponse response = MeiMapper.toResponse(mei);
        return response;
    }

    public ResponseMessage editar(Long idUsuario, MeiRequest request) {
        Mei mei = (Mei) BuscaPorId(idUsuario, repository);

        if (mei == null)
            throw new RuntimeException("Usuário não encontrado.");

        mei.setNome(request.getNome());
        mei.setCnpj(request.getCnpj());
        mei.setEmail(request.getEmail());
        mei.setSenha(request.getSenha());
        mei.setAreaAtuacao(request.getAreaAtuacao());
        mei.setNomeEmpresa(request.getNomeEmpresa());

        repository.save(mei);

        return new ResponseMessage("Usuário editado com sucesso.");
    }

    public ResponseMessage excluir(Long idUsuario) {
        Mei mei = (Mei) BuscaPorId(idUsuario, repository);

        if (mei == null)
            throw new RuntimeException("Usuário não encontrado.");

        repository.delete(mei);
        return new ResponseMessage("Usuário deletado com sucesso!");
    }

    public ResponseMessage gerarRelatorioMensalDeReceitasBrutas(Long idUsuario ){
        Mei mei = (Mei) BuscaPorId(idUsuario, repository);

        if (mei == null)
            throw new RuntimeException("Usuário não encontrado.");

        AreaAtuacao categoria = mei.getAreaAtuacao();
        ReceitaBrutaMensal target = new ReceitaBrutaMensal();

        target.setCategoria(categoria);

        LocalDate dataDeInicio = LocalDate.of(2023, 03, 5);
        LocalDate dataFinal = LocalDate.of(2023, 04, 8);


        List<ReceitaBrutaMensal> categorias = new ArrayList<>();
        RelatorioMensalReceitasBrutas das = new RelatorioMensalReceitasBrutas();


        return new ResponseMessage("Por enquanto retorna isso.");
    }

    public void validaUsuarioJaCadastrado(MeiRequest request){
        Optional<Mei> meiSalvo = repository.findByEmail(request.getEmail());

        if(meiSalvo.isPresent())
            throw new RuntimeException("E-mail ou CNPJ informado já possui registros.");

        if(meiSalvo == null && request.getCnpj() != null)
            meiSalvo = repository.findByCnpj((request.getCnpj()));

        if(meiSalvo.isPresent())
            throw new RuntimeException("E-mail ou CNPJ informado já possui registros.");
    }

    private T BuscaPorId(Long idItem, JpaRepository repo) {
        Optional<T> optional = repo.findById(idItem);

        if (optional.isPresent())
            return optional.get();

        return null;
    }

}

package com.helpmeiserver.service;

import com.helpmeiserver.controller.request.LembreteRequest;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.mapper.LembreteMapper;
import com.helpmeiserver.model.Lembrete;
import com.helpmeiserver.repository.LembreteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@Service
public class LembreteService {

    private final LembreteRepository lembreteRepository;
    private final AdminService adminService;

    public ResponseMessage cadastrarLembrete(LembreteRequest request, Long idResponsavel) {
        adminService.validaUsuarioAdmin(idResponsavel);

        Lembrete lembrete = LembreteMapper.toEntity(request);

        lembreteRepository.save(lembrete);

        return new ResponseMessage("Criado com sucesso!");
    }

    public ResponseMessage editarLembrete(LembreteRequest request, Long idLembrete, Long idResponsavel) {
        adminService.validaUsuarioAdmin(idResponsavel);

        Lembrete lembrete = lembreteRepository.findByIdLembrete(idLembrete);

        if (lembrete == null)
            throw new RuntimeException("Lembrete não encontrado.");

        lembrete.setData(request.getData());
        lembrete.setMensagem(request.getMensagem());
        lembrete.setTitulo(request.getTitulo());

        lembreteRepository.save(lembrete);

        return new ResponseMessage("Editado com sucesso!");
    }

    public List<Lembrete> buscarTodosLembretes(Long idResponsavel) {
        adminService.validaUsuarioAdmin(idResponsavel);

        return lembreteRepository.findAll();
    }

    public List<Lembrete> buscarLembretes() {

        Optional<List<Lembrete>> lembrete = Optional.ofNullable(lembreteRepository.findByDataBetween(LocalDate.now(), LocalDate.now().plusDays(7)));

        if(!lembrete.isPresent()){
            return new ArrayList<>();
        }

        return lembrete.get();
    }

    public ResponseMessage deletarLembrete(Long idLembrete, Long idResponsavel) {
        adminService.validaUsuarioAdmin(idResponsavel);

        Lembrete lembrete = lembreteRepository.findByIdLembrete(idLembrete);

        if (lembrete == null)
            throw new RuntimeException("Lembrete não encontrado.");

        lembreteRepository.delete(lembrete);

        return new ResponseMessage("Deletado com sucesso!");
    }

}

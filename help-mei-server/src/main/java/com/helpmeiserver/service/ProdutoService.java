package com.helpmeiserver.service;

import com.helpmeiserver.controller.request.ProdutoRequest;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.mapper.ProdutoMapper;
import com.helpmeiserver.model.Estoque;
import com.helpmeiserver.model.Mei;
import com.helpmeiserver.model.Produto;
import com.helpmeiserver.repository.EstoqueRepository;
import com.helpmeiserver.repository.MeiRepository;
import com.helpmeiserver.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private MeiRepository meiRepository;

    @Autowired
    private EstoqueRepository estoqueRepository;

    public ResponseMessage cadastrar(ProdutoRequest request, Long idMei) {
        Mei mei = meiRepository.findByIdUsuario(idMei).get();
        Estoque estoqueRegistrado = estoqueRepository.findByUsuario(mei);

        if (mei == null)
            throw new RuntimeException("Não foi possível encontrar o usuário.");

        if (estoqueRegistrado == null)
            throw new RuntimeException("Você não está autorizado.");

        Produto produto = ProdutoMapper.toEntity(request);
        Estoque estoque = estoqueRegistrado;
        produto.setEstoque(estoque);

        produtoRepository.save(produto);
        return new ResponseMessage("Criado com sucesso!");
    }

    public ResponseMessage editar(ProdutoRequest request, Long idMei, Long idProduto) {
        Mei mei = meiRepository.findByIdUsuario(idMei).get();
        Estoque estoqueRegistrado = estoqueRepository.findByUsuario(mei);
        Produto produto = produtoRepository.findByIdProduto(idProduto);

        if (mei == null)
            throw new RuntimeException("Não foi possível encontrar o usuário.");

        if (estoqueRegistrado == null)
            throw new RuntimeException("Você não está autorizado.");

        if (produto == null)
            throw new RuntimeException("Não foi possível encontrar o produto.");

        produto.setValorVenda(request.getValorVenda());
        produto.setNome(request.getNome());
        produto.setFornecedor(request.getFornecedor());
        produto.setValorProducao(request.getValorProducao());
        produto.setValorVenda(request.getValorVenda());

        produtoRepository.save(produto);
        return new ResponseMessage("Editado com sucesso!");
    }

    public List<Produto> buscarTodosPorMei (Long idMei) {
        Mei mei = meiRepository.findByIdUsuario(idMei).get();
        Estoque estoque = estoqueRepository.findByUsuario(mei);

        if (mei == null)
            throw new RuntimeException("Não foi possível encontrar o usuário.");

        if (estoque == null)
            throw new RuntimeException("Você não está autorizado.");

        return produtoRepository.findByEstoque(estoque);
    }

    public Produto buscarPorId (Long idMei, Long idProduto) {
        Mei mei = meiRepository.findByIdUsuario(idMei).get();
        Produto produto = produtoRepository.findByIdProdutoAndEstoqueUsuario(idProduto, mei);

        if (mei == null)
            throw new RuntimeException("Não foi possível encontrar o usuário.");

        if (produto == null)
            throw new RuntimeException("Você não está autorizado.");

        return produto;
    }

    public ResponseMessage deletar (Long idMei, Long idProduto) {
        Mei mei = meiRepository.findByIdUsuario(idMei).get();
        Produto produto = produtoRepository.findByIdProdutoAndEstoqueUsuario(idProduto, mei);

        if (mei == null)
            throw new RuntimeException("Não foi possível encontrar o usuário.");

        if (produto == null)
            throw new RuntimeException("Você não está autorizado.");

        produtoRepository.deleteById(idProduto);

        return new ResponseMessage("Editado com sucesso!");
    }
}

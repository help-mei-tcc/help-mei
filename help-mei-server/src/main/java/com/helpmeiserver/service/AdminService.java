package com.helpmeiserver.service;

import com.helpmeiserver.constants.Constants;
import com.helpmeiserver.controller.request.MeiRequest;
import com.helpmeiserver.controller.response.ResponseMessage;
import com.helpmeiserver.mapper.MeiMapper;
import com.helpmeiserver.model.Admin;
import com.helpmeiserver.model.Mei;
import com.helpmeiserver.model.Permissao;
import com.helpmeiserver.repository.AdminRepository;
import com.helpmeiserver.repository.LembreteRepository;
import com.helpmeiserver.repository.MeiRepository;
import com.helpmeiserver.repository.PermissaoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@Service
public class AdminService {

    private final MeiService meiService;
    private final AdminRepository adminRepository;

    private final PermissaoRepository permissaoRepository;

    private final MeiRepository meiRepository;

    private final LembreteRepository lembreteRepository;

    public ResponseMessage cadastrarMei(MeiRequest request, Long idResponsavel) {
        validaUsuarioAdmin(idResponsavel);

        meiService.validaUsuarioJaCadastrado(request);

        Mei mei = MeiMapper.toEntity(request);
        meiRepository.save(mei);

        return new ResponseMessage("Criado com sucesso!");
    }

    public ResponseMessage editarMei(MeiRequest request, Long idMei, Long idResponsavel) {
        validaUsuarioAdmin(idResponsavel);

        Mei mei = meiRepository.findByIdUsuario(idMei).get();

        if (mei == null)
            throw new RuntimeException("Usuário MEI não encontrado.");

        mei.setNome(request.getNome());
        mei.setCnpj(request.getCnpj());
        mei.setEmail(request.getEmail());
        mei.setSenha(request.getSenha());
        mei.setAreaAtuacao(request.getAreaAtuacao());
        mei.setNomeEmpresa(request.getNomeEmpresa());

        meiRepository.save(mei);

        return new ResponseMessage("Editado com sucesso!");
    }

    public List<Mei> buscarTodosMei(Long idResponsavel) {
        validaUsuarioAdmin(idResponsavel);

        return meiRepository.findAll();
    }

    public ResponseMessage deletarMei(Long idMei, Long idResponsavel) {
        validaUsuarioAdmin(idResponsavel);

        Mei mei = meiRepository.findByIdUsuario(idMei).get();

        if (mei == null)
            throw new RuntimeException("Usuário MEI não encontrado.");

        meiRepository.delete(mei);

        return new ResponseMessage("Deletado com sucesso!");
    }

    public void validaUsuarioAdmin(Long idResponsavel) {
        Admin admin = adminRepository.findByIdUsuario(idResponsavel);

        if (admin == null)
            throw new RuntimeException("Não foi possível encontrar o usuário.");
    }
}

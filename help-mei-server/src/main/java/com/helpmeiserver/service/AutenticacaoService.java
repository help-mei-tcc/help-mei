package com.helpmeiserver.service;

import com.helpmeiserver.controller.request.AutenticacaoRequest;
import com.helpmeiserver.controller.request.CadastroRequest;
import com.helpmeiserver.controller.response.UsuarioBasicoResponse;
import com.helpmeiserver.model.Mei;
import com.helpmeiserver.model.Usuario;
import com.helpmeiserver.model.enums.Role;
import com.helpmeiserver.repository.MeiRepository;
import com.helpmeiserver.repository.UsuarioRepository;

import java.util.ArrayList;
import java.util.Optional;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AutenticacaoService {

    private final MeiRepository meiRepository;
    private final UsuarioRepository usuarioRepository;

    public UsuarioBasicoResponse efetuarLogin(AutenticacaoRequest request) {
        Usuario usuario = usuarioRepository
                .findByEmail(request.getEmail())
                .orElseThrow(() -> new RuntimeException("Email ou senha incorretos."));

        if (!request.getSenha().equals(usuario.getSenha()))
            throw new RuntimeException("Email ou senha incorretos.");

        Optional<Mei> usuarioMeiOptional = meiRepository.findByIdUsuario(usuario.getIdUsuario());

        if (!usuarioMeiOptional.isPresent())
            return UsuarioBasicoResponse.builder()
                    .idMei(usuario.getIdUsuario())
                    .email(usuario.getEmail())
                    .build();

        Mei usuarioMei = usuarioMeiOptional.get();

        return UsuarioBasicoResponse.builder()
                .idMei(usuario.getIdUsuario())
                .email(usuarioMei.getEmail())
                .nome(usuarioMei.getNome())
                .nomeEmpresa(usuarioMei.getNomeEmpresa())
                .areaAtuacao(usuarioMei.getAreaAtuacao())
                .cnpj(usuarioMei.getCnpj())
                .senha(usuarioMei.getSenha())
                .build();
    }

    public UsuarioBasicoResponse efetuarCadastro(CadastroRequest request) {

        boolean emailJaCadastrado = usuarioRepository
                .findByEmail(request.getCredenciais().getEmail())
                .isPresent();

        if (emailJaCadastrado)
            throw new RuntimeException("Email já cadastrado.");

        Mei usuarioMei = Mei.builder()
                .role(Role.USER)
                .email(request.getCredenciais().getEmail())
                .senha(request.getCredenciais().getSenha())
                .cnpj(request.getCnpj())
                .nome(request.getNome())
                .areaAtuacao(request.getAreaAtuacao())
                .nomeEmpresa(request.getNomeEmpresa())
                .notasFiscais(new ArrayList<>())
                .relatorios(new ArrayList<>())
                .build();

        Usuario usuarioNovo = usuarioRepository.save(usuarioMei);

        return UsuarioBasicoResponse.builder()
                .idMei(usuarioNovo.getIdUsuario())
                .email(usuarioMei.getEmail())
                .nome(usuarioMei.getNome())
                .nomeEmpresa(usuarioMei.getNomeEmpresa())
                .areaAtuacao(usuarioMei.getAreaAtuacao())
                .cnpj(usuarioMei.getCnpj())
                .build();
    }
}

package com.helpmeiserver.service;

import com.helpmeiserver.model.Movimentacao;
import com.helpmeiserver.model.Pedido;
import com.helpmeiserver.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

   public Pedido cadastrarPedido(Pedido entidade){
       return pedidoRepository.save(entidade);
   }
}

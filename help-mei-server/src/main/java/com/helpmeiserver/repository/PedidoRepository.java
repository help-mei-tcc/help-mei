package com.helpmeiserver.repository;

import com.helpmeiserver.model.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    @Override
    Optional<Pedido> findById(Long aLong);

}
package com.helpmeiserver.repository;

import com.helpmeiserver.model.Estoque;
import com.helpmeiserver.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstoqueRepository extends JpaRepository<Estoque, Long> {

    Estoque findByIdEstoque(long idEstoque);

    Estoque findByUsuario (Usuario responsavel);

}

package com.helpmeiserver.repository;

import com.helpmeiserver.model.Estoque;
import com.helpmeiserver.model.Produto;
import com.helpmeiserver.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    Produto findByIdProduto (long idProduto);
    List<Produto> findByEstoque (Estoque estoque);
    Produto findByIdProdutoAndEstoqueUsuario (Long idProduto, Usuario usuario);
}

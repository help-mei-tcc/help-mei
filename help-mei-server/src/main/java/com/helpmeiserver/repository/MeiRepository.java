package com.helpmeiserver.repository;

import com.helpmeiserver.model.Mei;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MeiRepository extends JpaRepository<Mei, Long> {

    Optional<Mei> findByIdUsuario (Long id);
    Optional<Mei> findByCnpj (String cnpj);
    Optional<Mei> findByEmail (String email);

    List<Mei> findAll ();

}
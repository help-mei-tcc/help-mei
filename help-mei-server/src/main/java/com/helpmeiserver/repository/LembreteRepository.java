package com.helpmeiserver.repository;

import com.helpmeiserver.model.Lembrete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface LembreteRepository extends JpaRepository<Lembrete, Long> {

    Lembrete findByIdLembrete (Long idLembrete);
    List<Lembrete> findAll ();
    List<Lembrete> findByDataBetween(LocalDate dataExposicao, LocalDate dataLimite);

}
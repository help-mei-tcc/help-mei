package com.helpmeiserver.repository;

import com.helpmeiserver.model.Permissao;
import com.helpmeiserver.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Long> {

    List<Permissao> findByUsuariosIdUsuario (Long idUsuario);
}
package com.helpmeiserver.repository;

import com.helpmeiserver.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {

    Admin findByIdUsuario(Long id);
}
package com.helpmeiserver.repository;

import com.helpmeiserver.model.Usuario;
import com.helpmeiserver.model.UsuarioSessao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioSessaoRepository extends JpaRepository<UsuarioSessao, Long> {

    UsuarioSessao findByUsuario (Usuario usuario);
}
package com.helpmeiserver.repository;

import com.helpmeiserver.model.NotaFiscal;
import com.helpmeiserver.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotaFiscalRepository extends JpaRepository<NotaFiscal, Long> {

    NotaFiscal findByUsuario(Usuario usuario);

}

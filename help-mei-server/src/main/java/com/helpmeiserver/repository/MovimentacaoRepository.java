package com.helpmeiserver.repository;

import com.helpmeiserver.model.Mei;
import com.helpmeiserver.model.Movimentacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovimentacaoRepository extends JpaRepository<Movimentacao, Long> {

    Optional<Movimentacao> findByIdMovimentacaoAndUsuarioIdUsuario(Long idMovimentacao, Long idUsuario);

    List<Movimentacao> findByUsuarioIdUsuario(Long idUsuario);
}

import { createGlobalState } from "react-hooks-global-state";

export const defaultValue = {
  currentUser: {
    idUsuario: "",
    nome: "",
    email: "",
    cnpj: "",
    nomeEmpresa: "",
    areaAtuacao: "",
  },
};

const { setGlobalState, useGlobalState } = createGlobalState(defaultValue);

export { useGlobalState, setGlobalState };

// localStorage.removeItem("currentUser");
// localStorage.setItem("currentUser", JSON.stringify(value))
// JSON.parse(localStorage.getItem("currentUser"))

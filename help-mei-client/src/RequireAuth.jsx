import { Navigate } from "react-router-dom";

export function RequireAuth({ children }) {
  const localStorageUser = JSON.parse(localStorage.getItem("currentUser"));
  
  if (!!localStorageUser?.idUsuario) {
    return children
  };

  return <Navigate to={"/"} replace={true} />;
}

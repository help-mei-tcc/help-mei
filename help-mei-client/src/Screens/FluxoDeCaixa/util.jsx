import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

export const tipoMovimentacaoEntrada = "Entrada";
export const tipoMovimentacaoSaida = "Saída";

const schema = yup
  .object({
    descricao: yup.string().required("O campo é obrigatório."),
    categoriaMovimentacao: yup.string().required("O campo é obrigatório."),
    tipoMovimentacao: yup.string().required("O campo é obrigatório."),
    valor: yup
      .number()
      .test(
        "NAN",
        "O valor precisa ser numérico",
        (value) => !isNaN(value)
      )
      .test(
        "diferenteDeZero",
        "O valor precisa ser diferente de zero",
        (value) => value !== 0
      )
      .required("O campo é obrigatório."),
  })
  .required("Ocampo é obrigatório.");

export const formDefaultConfig = {
  resolver: yupResolver(schema),
  reValidateMode: "onChange",
  criteriaMode: "firstError",
  defaultValues: {
    descricao: "",
    categoriaMovimentacao: "",
    tipoMovimentacao: "ENTRADA",
    valor: 0,
  },
};

export const categoriaOptions = [
  {
    value: "AGUA_E_LUZ",
    label: "Água e luz",
  },
  {
    value: "ALUGUEL",
    label: "Aluguel",
  },
  {
    value: "APLICACOES_FINANCEIRAS",
    label: "Aplicações financeiras",
  },
  {
    value: "COMBUSTIVEL",
    label: "Combustível",
  },
  {
    value: "EMPRESTIMO",
    label: "Empréstimo",
  },
  {
    value: "FORNECEDORES",
    label: "Fornecedores",
  },
  {
    value: "FRETE",
    label: "Frete",
  },
  {
    value: "IMPOSTOS_E_TAXAS",
    label: "Impostos e taxas",
  },
  {
    value: "INVESTIMENTO_EXTERNO",
    label: "Investimento externo",
  },
  {
    value: "INVESTIMENTO_INTERNO",
    label: "Aluguel",
  },
  {
    value: "PRESENTE",
    label: "Presente",
  },
  {
    value: "PRO_LABORE",
    label: "Pró-labore",
  },
  {
    value: "SERVICOS_CONTRATADOS",
    label: "Serviços contratados",
  },
  {
    value: "SERVICOS_PRESTADOS",
    label: "Serviços prestados",
  },
  {
    value: "TELEFONE_E_INTERNET",
    label: "Telefone e internet",
  },
  {
    value: "VENDAS",
    label: "Vendas",
  },
  {
    value: "OUTROS",
    label: "Outros",
  },
];

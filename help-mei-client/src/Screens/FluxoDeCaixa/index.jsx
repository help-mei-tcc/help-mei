import "./style.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Controller, set, useForm } from "react-hook-form";
import { alertDefaultConfig } from "../../Utils/utils";
import { useFluxoDeCaixa } from "../../Axios/FluxoDeCaixa";
import { DefaultHeader } from "../../Components/DefaultHeader";
import { formDefaultConfig, categoriaOptions } from "./util";
import { useGlobalState, setGlobalState } from "../../GlobalState";
import { MovimentacaoItem } from "../../Components/MovimentacaoItem";
import {
  Alert,
  Button,
  FormControl,
  FormControlLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  LinearProgress,
  MenuItem,
  OutlinedInput,
  Radio,
  RadioGroup,
  TextField,
} from "@mui/material";

export function FluxoDeCaixa() {
  const navigator = useNavigate();

  const [update, setUpdate] = useState(false);
  const [editar, setEditar] = useState(false);
  const [loader, setLoader] = useState(false);
  const [valorTotal, setValorTotal] = useState(0);
  const [currentUser] = useGlobalState("currentUser");
  const [movimentacoes, setMovimentacoes] = useState([]);
  const [alertConfig, setAlertConfig] = useState(alertDefaultConfig);

  const {
    postCadastrarMovimentacao,
    getBuscarMovimentacoes,
    deleteDeletarMovimentacoes,
    postEditarMovimentacao,
  } = useFluxoDeCaixa();

  const {
    reset,
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm(formDefaultConfig);

  useEffect(() => {
    setUpdate(true);
  }, []);

  useEffect(() => {
    async function verifyCurrentUser() {
      const localStorageUser = JSON.parse(localStorage.getItem("currentUser"));

      if (localStorageUser?.idUsuario == "") {
        navigator("/");
      }

      if (currentUser?.idUsuario == "") {
        setGlobalState("currentUser", localStorageUser);
      }

      setLoader(true);
      const response = await getBuscarMovimentacoes(
        localStorageUser?.idUsuario
      );
      setMovimentacoes(response?.data);
      setLoader(false);
    }

    verifyCurrentUser();
  }, [update]);

  useEffect(() => {
    function calculaValorTotal() {
      if (valorTotal !== 0) {
        setValorTotal(0);
      }

      let initialValue = 0;

      movimentacoes.map((movimentacao) => {
        if (movimentacao.tipoMovimentacao === "ENTRADA") {
          initialValue += movimentacao.valor;
        } else {
          initialValue -= movimentacao.valor;
        }
      });

      setValorTotal(initialValue);
    }

    calculaValorTotal();
  }, [movimentacoes]);

  async function onSubmit(data) {
    try {
      const request = {
        descricao: data?.descricao,
        idUsuario: currentUser.idUsuario,
        idPedido: null,
        categoria: data?.categoriaMovimentacao,
        tipoMovimentacao: data?.tipoMovimentacao,
        valor: data?.valor,
        listaProdutos: [],
      };

      setLoader(true);
      {
        editar
          ? await postEditarMovimentacao(
              request,
              currentUser?.idUsuario,
              editar.idMovimentacao
            )
          : await postCadastrarMovimentacao(request);
      }
      setLoader(false);

      setAlertConfig({
        show: true,
        message: "Sucesso!",
        severity: "success",
      });
      reset();
      setUpdate(!update);
    } catch (error) {
      if (error?.response?.status > 299) {
        setAlertConfig({
          show: true,
          message: error?.response?.data?.message,
          severity: "error",
        });
      }
    }
  }

  function handleEditar(idMovimentacao) {
    const movimentacao = movimentacoes.find(
      (movimentacao) => movimentacao.idMovimentacao === idMovimentacao
    );

    const movimentacaoState = {
      mostrar: true,
      idMovimentacao: movimentacao.idMovimentacao,

      descricao: movimentacao.descricao,
      idUsuario: currentUser.idUsuario,
      idPedido: null,
      categoria: movimentacao.categoria,
      valor: movimentacao.valor,
      tipoMovimentacao: movimentacao.tipoMovimentacao,
      listaProdutos: [],
    };

    setValue("descricao", movimentacaoState?.descricao);
    setValue("tipoMovimentacao", movimentacaoState?.tipoMovimentacao);
    setValue("valor", movimentacaoState?.valor);
    setValue(
      "categoriaMovimentacao",
      categoriaOptions.find(
        (movimentacao) => movimentacao.label === movimentacaoState?.categoria
      ).value
    );

    setEditar(movimentacaoState);
    setUpdate(!update);
  }

  async function handleExcluir(idMovimentacao) {
    setLoader(true);
    await deleteDeletarMovimentacoes(idMovimentacao, currentUser?.idUsuario);
    setUpdate(!update);
    setLoader(false);
  }

  function onBack() {
    reset();
    setEditar("");
  }

  function handleShowInfo(){
    
  }

  return (
    <div className="caixa-page">
      <DefaultHeader nomeEmpresa={currentUser?.nomeEmpresa} />

      <div className="caixa-body">
        <div className="caixa-box-header">
          <p className="caixa-header-text">FLUXO DE CAIXA</p>
        </div>

        <div className="caixa-box-body">
          <div className="caixa-controler">
            <p className="caixa-controler-title">
              {editar ? "Modificar movimentação" : "Inserir nova movimentação"}
            </p>

            <form
              className="movimentacao-form"
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="form-inputs-box">
                <Controller
                  name={"categoriaMovimentacao"}
                  control={control}
                  render={({ field: { onChange, value }, formState }) => (
                    <FormControl
                      variant="outlined"
                      className="form-input-box type-tree"
                      error={!!formState.errors?.categoriaMovimentacao}
                    >
                      <TextField
                        label="Categoria movimentacao"
                        select
                        className="form-input"
                        value={value}
                        onChange={onChange}
                        error={!!formState.errors?.categoriaMovimentacao}
                      >
                        {categoriaOptions.map((option) => (
                          <MenuItem
                            className="menu-item"
                            key={option?.value}
                            value={option?.value}
                          >
                            {option?.label}
                          </MenuItem>
                        ))}
                      </TextField>
                      {!!formState.errors?.categoriaMovimentacao && (
                        <FormHelperText>
                          {errors?.categoriaMovimentacao?.message}
                        </FormHelperText>
                      )}
                    </FormControl>
                  )}
                />

                <div className="form-input-box">
                  <Controller
                    name={"descricao"}
                    control={control}
                    render={({ field: { onChange, value }, formState }) => (
                      <TextField
                        label="Descricao"
                        className="form-input"
                        variant="outlined"
                        autoComplete="off"
                        value={value}
                        onChange={onChange}
                        error={!!formState.errors?.descricao}
                        helperText={errors?.descricao?.message}
                      />
                    )}
                  />
                </div>

                <div className="form-input-box">
                  <Controller
                    name={"valor"}
                    control={control}
                    render={({ field: { onChange, value }, formState }) => (
                      <FormControl
                        variant="outlined"
                        error={!!formState.errors?.valor}
                      >
                        <InputLabel
                          className="form-input"
                          htmlFor="movimentacao-id"
                        >
                          Valor
                        </InputLabel>
                        <OutlinedInput
                          id="movimentacao-id"
                          type="number"
                          label="Valor"
                          variant="outlined"
                          value={value}
                          onChange={onChange}
                          startAdornment={
                            <InputAdornment position="start">R$</InputAdornment>
                          }
                        />
                        {!!formState.errors?.valor && (
                          <FormHelperText>
                            {errors?.valor?.message}
                          </FormHelperText>
                        )}
                      </FormControl>
                    )}
                  />
                </div>

                <div className="movimentacao-form-switches-box">
                  <p className="titulo-tipo-movimentacao">
                    Escolha a natureza da movimentação
                  </p>
                  <Controller
                    name={"tipoMovimentacao"}
                    control={control}
                    render={({ field: { onChange, value }, formState }) => (
                      <>
                        <RadioGroup
                          className="radio-group-tipo-entrada"
                          row
                          value={value}
                          onChange={onChange}
                          helpertext={errors?.tipoMovimentacao?.message}
                        >
                          <FormControlLabel
                            value="ENTRADA"
                            control={<Radio />}
                            label="Entrada"
                          />
                          <FormControlLabel
                            value="SAIDA"
                            control={<Radio />}
                            label="Saída"
                          />
                        </RadioGroup>
                      </>
                    )}
                  />
                </div>
              </div>

              {!!alertConfig.show && (
                <div className="movimentacao-card-alert">
                  <Alert
                    severity={alertConfig.severity}
                    variant="filled"
                    action={
                      <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                          setAlertConfig({ ...alertConfig, show: false });
                        }}
                      >
                        X
                      </IconButton>
                    }
                  >
                    {alertConfig?.message}
                  </Alert>
                </div>
              )}

              <div className="movimentacao-form-button-group">
                <Button variant="outlined" onClick={() => onBack()}>
                  Cancelar
                </Button>
                <Button variant="contained" type="submit">
                  {editar ? "Atualizar" : "Cadastrar"}
                </Button>
              </div>
            </form>
          </div>

          <div className="caixa-menu">
            <div className="movimentacao-card">
              <div className={`card-color black`}></div>
              <div className="card-descriptrion">Descrição</div>
              <div className="card-category">Categoria</div>
              <div className="card-date">Data de criação</div>
              <div className={`card-value`}>Valor em R$</div>
              <p className="cart-text-word">Editar</p>
              <p className="cart-text-word">Excluir</p>
            </div>
            <div className="caixa-lista">
              {loader && <LinearProgress />}
              {movimentacoes?.map((movimentacao, key) => (
                <MovimentacaoItem
                  key={key}
                  idMovimentacao={movimentacao?.idMovimentacao}
                  pedido={movimentacao?.pedido}
                  valor={movimentacao?.valor}
                  descricao={movimentacao?.descricao}
                  dataInsercao={movimentacao?.dataInsercao}
                  categoria={movimentacao?.categoria}
                  tipoMovimentacao={movimentacao?.tipoMovimentacao}
                  onEditar={handleEditar}
                  onExcluir={handleExcluir}
                />
              ))}
            </div>

            <div className="caixa-footer">
              <p className="footer-title">Total:</p>
              <div className={`footer-value-box ${valorTotal > 0 ? 'fontGreen': 'fontRed'}`} >
                <p>R$ {Math.round(valorTotal)}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

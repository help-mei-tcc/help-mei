import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const schema = yup
  .object({
    login: yup
      .string()
      .email("O email deve ser válido")
      .test(
        "tamanho máximo",
        "O campo deve possuir no máximo 100 caracteres",
        (email) => email != null && email.length <= 100
      )
      .required("O campo é obrigatório"),
    password: yup
      .string()
      .test(
        "tamanho mínimo",
        "A senha deve possuir no mínimo 8 caracteres",
        (senha) => senha != null && senha.length >= 8
      )
      .test(
        "tamanho máximo",
        "A senha deve possuir no máximo 25 caracteres",
        (senha) => senha != null && senha.length <= 25
      )
      .required("O campo é obrigatório."),
  })
  .required();

const formDefaultConfig = {
  resolver: yupResolver(schema),
  reValidateMode: "onChange",
  criteriaMode: "firstError",
  defaultValues: {
    login: "",
    password: "",
  },
};

export { formDefaultConfig };

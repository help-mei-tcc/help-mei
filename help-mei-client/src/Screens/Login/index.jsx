import "./style.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Controller, useForm } from "react-hook-form";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { formDefaultConfig } from "./util";
import { alertDefaultConfig } from "../../Utils/utils";
import full_logo from "../../Assets/Images/full_logo.png";
import { useAutenticacao } from "../../Axios/Autenticacao";
import { useGlobalState, setGlobalState } from "../../GlobalState";
import {
  Button,
  TextField,
  Alert,
  InputAdornment,
  IconButton,
  OutlinedInput,
  InputLabel,
  FormControl,
  FormHelperText,
} from "@mui/material";

export function LoginScreen() {
  const navigate = useNavigate();
  const [currentUser] = useGlobalState("currentUser");

  const { postLogin } = useAutenticacao();
  const [showPassword, setShowPassword] = useState(false);
  const [alertConfig, setAlertConfig] = useState(alertDefaultConfig);

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm(formDefaultConfig);

  useEffect(() => {
    function verifyIfUserIsLogged() {
      const localStorageUser = JSON.parse(localStorage.getItem("currentUser"));

      if(!!localStorageUser?.idUsuario){
        navigate("home")
      }
    }

    verifyIfUserIsLogged();
  }, []);

  function handleClickShowPassword() {
    setShowPassword(!showPassword);
  }

  function setNewUserInformation(data) {
    const userInformation = {
      idUsuario: data?.idMei,
      nome: data?.nome,
      email: data?.email,
      cnpj: data?.cnpj,
      nomeEmpresa: data?.nomeEmpresa,
      areaAtuacao: data?.areaAtuacao,
    };

    setGlobalState("currentUser", userInformation);
    localStorage.setItem("currentUser", JSON.stringify(userInformation));
  }

  async function onSubmit(data) {
    if (!errors.login && !errors.password) {
      const requestBody = {
        email: data?.login,
        senha: data?.password,
      };

      try {
        const response = await postLogin(requestBody);

        setNewUserInformation(response?.data);
        navigate("/home");
      } catch (error) {
        if (error?.response?.status > 299) {
          setAlertConfig({
            show: true,
            message: error?.response?.data?.message,
            severity: "error",
          });

          reset();
        }
      }
    }
  }

  return (
    <div className="login-page">
      <div className="login-card">
        <div className="login-card-container">
          <div className="login-card-image">
            <img src={full_logo} alt="imagem" className="logo-image" />
          </div>

          {!!alertConfig.show && (
            <div className="login-card-alert">
              <Alert severity={alertConfig.severity} className="card-alert">
                {alertConfig?.message}
              </Alert>
            </div>
          )}

          <form className="login-card-form" onSubmit={handleSubmit(onSubmit)}>
            <div className="form-inputs">
              <div className="form-input-box">
                <Controller
                  name={"login"}
                  control={control}
                  render={({ field: { onChange, value }, formState }) => (
                    <TextField
                      label="Email"
                      className="form-input"
                      variant="outlined"
                      autoComplete="off"
                      value={value}
                      onChange={onChange}
                      error={!!formState.errors?.login}
                      helperText={errors?.login?.message}
                    />
                  )}
                />
              </div>
              <div className="form-input-box">
                <Controller
                  name={"password"}
                  control={control}
                  render={({ field: { onChange, value }, formState }) => (
                    <FormControl
                      variant="outlined"
                      error={!!formState.errors?.password}
                    >
                      <InputLabel htmlFor="login-senha">Senha</InputLabel>
                      <OutlinedInput
                        id="login-senha"
                        label="Senha"
                        className="form-input"
                        variant="outlined"
                        autoComplete="off"
                        value={value}
                        onChange={onChange}
                        type={showPassword ? "text" : "password"}
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton onClick={handleClickShowPassword}>
                              {showPassword ? (
                                <VisibilityOff />
                              ) : (
                                <Visibility />
                              )}
                            </IconButton>
                          </InputAdornment>
                        }
                        error={!!formState.errors?.password}
                      />
                      {!!formState.errors?.password && (
                        <FormHelperText error>
                          {errors?.password?.message}
                        </FormHelperText>
                      )}
                    </FormControl>
                  )}
                />
              </div>
            </div>

            <div className="form-buttons">
              <Button
                variant="contained"
                size="large"
                type="submit"
                className="entrar-button"
              >
                Entrar
              </Button>
            </div>

            <div className="divisory-bar"></div>

            <div className="form-buttons">
              <p className="register-text">Não possui uma conta?</p>
              <Button
                variant="outlined"
                size="medium"
                onClick={() => {
                  navigate("/cadastro");
                }}
              >
                Criar uma conta
              </Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

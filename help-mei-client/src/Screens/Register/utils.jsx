import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const schema = yup
  .object({
    nome: yup
      .string()
      .test(
        "tamanho máximo",
        "O nome deve possuir no máximo 60 caracteres",
        (nome) => nome != null && nome.length <= 60
      )
      .required("O campo é obrigatório."),
    email: yup
      .string()
      .test(
        "tamanho máximo",
        "O email deve possuir no máximo 30 caracteres",
        (email) => email != null && email.length <= 30
      )
      .email("O email deve ser válido")
      .required("O campo é obrigatório."),
    cnpj: yup
      .string()
      .test(
        "curto",
        "O cnpj deve ter 14 dígitos",
        (value) => value != null && value.length != 14
      )
      .required("O campo é obrigatório."),
    nomeEmpresa: yup.string().required("O campo é obrigatório."),
    areaAtuacao: yup.string().required("O campo é obrigatório."),
    senha: yup
      .string()
      .test(
        "tamanho mínimo",
        "A senha deve possuir no mínimo 8 caracteres",
        (senha) => senha != null && senha.length >= 8
      )
      .test(
        "tamanho máximo",
        "A senha deve possuir no máximo 25 caracteres",
        (senha) => senha != null && senha.length <= 25
      )
      .required("O campo é obrigatório."),
    repetirSenha: yup
      .string()
      .test(
        "tamanho mínimo",
        "A senha deve possuir no mínimo 8 caracteres",
        (senha) => senha != null && senha.length >= 8
      )
      .test(
        "tamanho máximo",
        "A senha deve possuir no máximo 25 caracteres",
        (senha) => senha != null && senha.length <= 25
      )
      .oneOf([yup.ref("senha"), null], "As senhas devem coincidir."),
  })
  .required("O campo é obrigatório.");

const formDefaultConfig = {
  resolver: yupResolver(schema),
  reValidateMode: "onChange",
  criteriaMode: "firstError",
  defaultValues: {
    nome: "",
    email: "",
    cnpj: "",
    nomeEmpresa: "",
    areaAtuacao: "COMERCIO",
    senha: "",
    repetirSenha: "",
  },
};

export { formDefaultConfig };

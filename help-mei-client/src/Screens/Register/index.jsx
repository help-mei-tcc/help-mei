import "./style.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Controller, useForm } from "react-hook-form";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { formDefaultConfig } from "./utils";
import { alertDefaultConfig } from "../../Utils/utils";
import full_logo from "../../Assets/Images/full_logo.png";
import { useAutenticacao } from "../../Axios/Autenticacao";
import { useGlobalState, setGlobalState } from "../../GlobalState";
import { RegisterProgress } from "../../Components/RegisterProgress";
import {
  Alert,
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  InputAdornment,
  IconButton,
  OutlinedInput,
  InputLabel,
  Radio,
  RadioGroup,
  TextField,
  FormHelperText,
} from "@mui/material";

export function RegisterScreen() {
  const navigator = useNavigate();

  const { postCadastro } = useAutenticacao();
  const [registerStage, setRegisterStage] = useState(0);
  const [showPassword, setShowPassword] = useState(false);
  const [botaoCadastrar, setBotaoCadastrar] = useState("button");
  const [alertConfig, setAlertConfig] = useState(alertDefaultConfig);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const {
    handleSubmit,
    control,
    getValues,
    formState: { errors },
  } = useForm(formDefaultConfig);

  useEffect(() => {}, [botaoCadastrar]);

  function onStepFoward() {
    if (registerStage + 1 <= 2) {
      setRegisterStage(registerStage + 1);
    }
  }

  function onBack() {
    if (registerStage - 1 >= 0) {
      setRegisterStage(registerStage - 1);
    } else {
      navigator("/");
    }
  }

  function handleValidarBotaoCadastrar() {
    if (getValues("repetirSenha") != "") {
      setBotaoCadastrar("submit");
    }
  }

  function handleClickShowPassword() {
    setShowPassword(!showPassword);
  }

  function handleClickShowConfirmPassword() {
    setShowConfirmPassword(!showConfirmPassword);
  }

  function setNewUserInformation(data) {
    const newUserInformation = {
      idUsuario: data?.idMei,
      nome: data?.nome,
      email: data?.email,
      cnpj: data?.cnpj,
      nomeEmpresa: data?.nomeEmpresa,
      areaAtuacao: data?.areaAtuacao,
    };

    setGlobalState("currentUser", newUserInformation);
    localStorage.setItem("currentUser", JSON.stringify(newUserInformation));
  }

  async function onSubmit(data) {
    if (registerStage === 2 && !errors?.root?.message && data.senha !== "") {
      const credenciais = {
        email: data?.email,
        senha: data?.senha,
      };

      const newRegister = {
        credenciais: credenciais,
        nome: data?.nome,
        cnpj: data?.cnpj,
        nomeEmpresa: data?.nomeEmpresa,
        areaAtuacao: data?.areaAtuacao,
      };

      try {
        const response = await postCadastro(newRegister);

        setNewUserInformation(response?.data);
        navigator("/home");
      } catch (error) {
        if (error?.response?.status > 299) {
          setAlertConfig({
            show: true,
            message: error?.response?.data?.message,
            severity: "error",
          });
        }
      }
    }
  }

  return (
    <div className="register-page">
      <div className="register-card">
        <div className="register-card-container">
          <div className="register-card-image">
            <img src={full_logo} alt="imagem" className="logo-image" />
          </div>

          {!!alertConfig.show && (
            <div className="login-card-alert">
              <Alert severity={alertConfig.severity} className="card-alert">
                {alertConfig?.message}
              </Alert>
            </div>
          )}

          <p className="cadastro-titulo">Cadastro de nova Conta</p>

          <RegisterProgress stage={registerStage} />

          <form className="login-card-form" onSubmit={handleSubmit(onSubmit)}>
            <div className="form-inputs">
              {registerStage === 0 && (
                <>
                  <div className="form-input-box">
                    <Controller
                      name={"nome"}
                      control={control}
                      render={({ field: { onChange, value }, formState }) => (
                        <TextField
                          label="Nome completo"
                          className="register-form-input"
                          variant="outlined"
                          autoComplete="off"
                          value={value}
                          onChange={onChange}
                          error={!!formState.errors?.nome}
                          helperText={errors?.nome?.message}
                        />
                      )}
                    />
                  </div>

                  <div className="form-input-box">
                    <Controller
                      name={"email"}
                      control={control}
                      render={({ field: { onChange, value }, formState }) => (
                        <TextField
                          label="Email"
                          type="email"
                          className="register-form-input"
                          variant="outlined"
                          autoComplete="off"
                          onChange={onChange}
                          value={value}
                          error={!!formState.errors?.email}
                          helperText={errors?.email?.message}
                        />
                      )}
                    />
                  </div>

                  <div className="form-input-box">
                    <Controller
                      name={"cnpj"}
                      control={control}
                      render={({ field: { onChange, value }, formState }) => (
                        <TextField
                          label="CNPJ"
                          className="register-form-input"
                          variant="outlined"
                          autoComplete="off"
                          value={value}
                          onChange={onChange}
                          error={!!formState.errors?.cnpj}
                          helperText={errors?.cnpj?.message}
                        />
                      )}
                    />
                  </div>
                </>
              )}

              {registerStage === 1 && (
                <>
                  <div className="form-input-box">
                    <Controller
                      name={"nomeEmpresa"}
                      control={control}
                      render={({ field: { onChange, value }, formState }) => (
                        <TextField
                          label="Nome da sua empresa"
                          className="register-form-input"
                          variant="outlined"
                          autoComplete="off"
                          value={value}
                          onChange={onChange}
                          error={!!formState.errors?.nomeEmpresa}
                          helperText={errors?.nomeEmpresa?.message}
                        />
                      )}
                    />
                  </div>

                  <div className="form-input-box">
                    <FormControl className="rg-area-atuacao">
                      <FormLabel className="rg-area-atuacao">
                        Area de atuação
                      </FormLabel>
                      <Controller
                        className="rg-area-atuacao"
                        control={control}
                        name="areaAtuacao"
                        render={({ field }) => (
                          <RadioGroup
                            {...field}
                            row
                            className="rg-area-atuacao-group"
                          >
                            <FormControlLabel
                              label="Comercio"
                              value="COMERCIO"
                              control={<Radio />}
                            />
                            <FormControlLabel
                              label="Serviços"
                              value="SERVICOS"
                              control={<Radio />}
                            />
                            <FormControlLabel
                              label="Indústria"
                              value="INDUSTRIA"
                              control={<Radio />}
                            />
                          </RadioGroup>
                        )}
                      />
                    </FormControl>
                  </div>
                </>
              )}

              {registerStage === 2 && (
                <>
                  <Controller
                    name={"senha"}
                    control={control}
                    render={({ field: { onChange, value }, formState }) => (
                      <FormControl
                        variant="outlined"
                        className="form-input-box"
                        error={!!formState.errors?.password}
                      >
                        <InputLabel htmlFor="cadastro-senha">Senha</InputLabel>
                        <OutlinedInput
                          id="cadastro-senha"
                          label="Senha"
                          className="register-form-input"
                          variant="outlined"
                          autoComplete="off"
                          type={showPassword ? "text" : "password"}
                          value={value}
                          onChange={onChange}
                          error={!!formState.errors?.senha}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton onClick={handleClickShowPassword}>
                                {showPassword ? (
                                  <VisibilityOff />
                                ) : (
                                  <Visibility />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                        {!!formState.errors?.password && (
                          <FormHelperText error>
                            {errors?.senha?.message}
                          </FormHelperText>
                        )}
                      </FormControl>
                    )}
                  />

                  <div className="form-input-box">
                    <Controller
                      name={"repetirSenha"}
                      control={control}
                      render={({ field: { onChange, value }, formState }) => (
                        <FormControl
                          variant="outlined"
                          className="form-input-box"
                          error={!!formState.errors?.password}
                        >
                          <InputLabel htmlFor="cadastro-repetir-senha">
                            Confirme sua senha
                          </InputLabel>
                          <OutlinedInput
                            id="cadastro-repetir-senha"
                            label="Confirme sua senha"
                            className="register-form-input"
                            variant="outlined"
                            autoComplete="off"
                            type={showConfirmPassword ? "text" : "password"}
                            value={value}
                            onChange={onChange}
                            error={!!formState.errors?.senha}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  onClick={handleClickShowConfirmPassword}
                                >
                                  {showConfirmPassword ? (
                                    <VisibilityOff />
                                  ) : (
                                    <Visibility />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            }
                          />
                          {!!formState.errors?.password && (
                            <FormHelperText error>
                              {errors?.senha?.message}
                            </FormHelperText>
                          )}
                        </FormControl>
                      )}
                    />
                  </div>
                </>
              )}
            </div>

            <div className="register-form-buttons">
              <Button variant="outlined" size="large" onClick={onBack}>
                Voltar
              </Button>
              <Button
                variant="contained"
                size="large"
                type={botaoCadastrar}
                onClick={
                  registerStage === 2
                    ? handleValidarBotaoCadastrar
                    : onStepFoward
                }
              >
                {registerStage === 2 ? "Cadastrar" : "Avançar"}
              </Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

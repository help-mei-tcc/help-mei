import "./style.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import empresa from "../../Assets/Images/empresa.png";
import { DefaultHeader } from "../../Components/DefaultHeader";
import { useGlobalState, setGlobalState } from "../../GlobalState";

export function HomeScreen() {
  const navigator = useNavigate();
  const [currentUser] = useGlobalState("currentUser");

  useEffect(() => {
    function verifyCurrentUser() {
      const localStorageUser = JSON.parse(localStorage.getItem("currentUser"));

      if (localStorageUser?.idUsuario == "") {
        navigator("/");
      }

      if (currentUser?.idUsuario == "") {
        setGlobalState("currentUser", localStorageUser);
      }
    }

    verifyCurrentUser();
  }, [currentUser]);

  return (
    <div className="home-page">
      <DefaultHeader nomeEmpresa={currentUser?.nomeEmpresa} />
      <div className="home-body">
        <div className="wellcome-card">
          <div className="wellcome-image">
            <img
              src={empresa}
              alt="Foto de prédios de uma empresa."
              className="wellcome-card-image"
            />
          </div>

          <div className="wellcome-message-box">
            <p className="wellcome-message-title">Olá {currentUser?.nome}!</p>
            <p className="wellcome-message-text">
              Como pretende gerenciar a sua empresa hoje? 
            </p>
            <p className="wellcome-message-text">
              Utilize o menu acima para acessar as funcionalidades do Help MEI.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

import "./index.css";
import React from "react";
import ReactDOM from "react-dom/client";
import {
  BrowserRouter,
  createBrowserRouter,
  Navigate,
  Route,
  Routes,
} from "react-router-dom";

import { HomeScreen } from "./Screens/Home";
import { LoginScreen } from "./Screens/Login";
import { RegisterScreen } from "./Screens/Register";
import { RequireAuth } from "./RequireAuth";
import { FluxoDeCaixa } from "./Screens/FluxoDeCaixa";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route element={<LoginScreen />} path="/" />
        <Route
          element={
            <RequireAuth>
              <HomeScreen />
            </RequireAuth>
          }
          path="/home"
        />
        <Route
          element={
            <RequireAuth>
              <FluxoDeCaixa />
            </RequireAuth>
          }
          path="/fluxo-de-caixa"
        />
        <Route element={<RegisterScreen />} path="/cadastro" />

        <Route path="*" element={<Navigate to={"/"} replace={true} />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

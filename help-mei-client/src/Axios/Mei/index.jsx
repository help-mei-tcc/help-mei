import axios from "axios";
import { HttRequests } from "../Base";

export function useMei() {
  const { Get, Post, Put, Delete } = HttRequests();
  const defaultUrl = "http://localhost:8080/mei/";

  async function postCadastro(requestConfig) {
    return axios.post(defaultUrl, requestConfig);
  }

  return {
    postCadastro,
  };
}

import axios from "axios";
import { DEFAULT_URL } from "../../Constants/AxiosConstants";

const URL_AUTENTICACAO = DEFAULT_URL + "/autenticacao";

export function useAutenticacao() {

  async function postLogin(config) {
    return axios.post(URL_AUTENTICACAO + "/login", config);
  }

  async function postCadastro(config) {
    return axios.post(URL_AUTENTICACAO + "/cadastro", config);
  }

  return {
    postLogin,
    postCadastro
  };
}

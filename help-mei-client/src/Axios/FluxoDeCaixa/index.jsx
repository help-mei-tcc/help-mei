import axios from "axios";
import { DEFAULT_URL } from "../../Constants/AxiosConstants";

const URL_FLUXO_DE_CAIXA = DEFAULT_URL + "/fluxo-de-caixa";

export function useFluxoDeCaixa() {
  async function postCadastrarMovimentacao(requestConfig) {
    return axios.post(
      URL_FLUXO_DE_CAIXA + "/cadastrar/movimentacao",
      requestConfig
    );
  }

  async function postEditarMovimentacao(requestConfig, idUsuario, idMovimentacao) {
    return axios.put(
      URL_FLUXO_DE_CAIXA +
        `/editar/movimentacao/${idUsuario}/${idMovimentacao}`,
      requestConfig
    );
  }

  async function getBuscarMovimentacoes(idUsuario) {
    return axios.get(
      URL_FLUXO_DE_CAIXA + `/buscar/movimentacoes/usuario/${idUsuario}`
    );
  }

  async function deleteDeletarMovimentacoes(idMovimentacao, idUsuario) {
    return axios.delete(
      URL_FLUXO_DE_CAIXA +
        `/deletar/movimentacao/${idMovimentacao}/usuario/${idUsuario}`
    );
  }

  return {
    postCadastrarMovimentacao,
    getBuscarMovimentacoes,
    deleteDeletarMovimentacoes,
    postEditarMovimentacao
  };
}

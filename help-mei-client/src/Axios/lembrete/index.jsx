import axios from "axios";
import { DEFAULT_URL } from "../../Constants/AxiosConstants";

const URL_LEMBRETE = DEFAULT_URL + "/lembrete";

export function useLembrete() {
  async function getLembretes(requestConfig) {
    return axios.get(
        URL_LEMBRETE + "/buscar",
      requestConfig
    );
  }

  return {
    getLembretes,
  };
}
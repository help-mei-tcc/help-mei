import "./style.css";
import {
  Card,
  IconButton,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import full_logo from "../../Assets/Images/full_logo.png";
import { defaultValue, setGlobalState } from "../../GlobalState";
import {
  AccountCircleOutlined,
  LogoutOutlined,
  NotificationsActiveOutlined,
  NotificationsNoneOutlined,
} from "@mui/icons-material";
import { useEffect, useState } from "react";
import { useLembrete } from "../../Axios/lembrete";

export function DefaultHeader({ nomeEmpresa }) {
  const navigator = useNavigate();
  const { getLembretes } = useLembrete();
  const [lembretes, setLembretes] = useState([]);

  const [open, setOpen] = useState(false);

  function handleOpen() {
    setOpen(!open);
  }

  useEffect(() => {}, [open]);

  useEffect(() => {
    async function buscarLembretes() {
      const response = await getLembretes();

      if (!!response?.data) {
        setLembretes(response?.data);
      }
    }

    buscarLembretes();
  }, []);

  function handleLogOff() {
    setGlobalState("currentUser", defaultValue);
    localStorage.removeItem("currentUser");
    navigator("/");
  }

  function handleGoHome() {
    navigator("/home");
  }

  return (
    <header className="header">
      <div className="header-Container">
        <div className="header-presentation">
          <div className="header-card-image">
            <button className="header-image-button" onClick={handleGoHome}>
              <img src={full_logo} alt="imagem" className="header-image" />
            </button>
          </div>

          <div className="header-company-name">
            <div className="user-icon-image">
              <AccountCircleOutlined />
            </div>
            <p className="company-name">{nomeEmpresa}</p>
          </div>
        </div>

        <ul className="header-menu">
          <li className="header-menu-item">
            <button
              className="header-menu-item-button"
              onClick={() => navigator("/fluxo-de-caixa")}
            >
              Fluxo de Caixa
            </button>
          </li>
          <li className="header-menu-item">
            <button
              className="header-menu-item-button"
              onClick={() => navigator("/estoque")}
            >
              Estoque
            </button>
          </li>
          <li className="header-menu-item">
            <button
              className="header-menu-item-button"
              onClick={() => navigator("/relatorios")}
            >
              Relatórios
            </button>
          </li>
        </ul>

        {lembretes?.map((value, key) => (
          <div className="car-lembrete-card" hidden={!open}>
            <Card hidden={!open} variant="outlined">
              <div className="lembrete-card">
                <div className="lembrete-card-titulo">{value?.titulo}</div>
                <div className="lembrete-card-texto">{value?.mensagem} </div>
              </div>
            </Card>
          </div>
          ))
        }

        <div className="header-buttons">
          <IconButton
            color="primary"
            aria-label="lembretes"
            onClick={handleOpen}
          >
            {lembretes?.length > 0 ? (
              <NotificationsActiveOutlined />
            ) : (
              <NotificationsNoneOutlined />
            )}
          </IconButton>

          <IconButton aria-label="logout" onClick={handleLogOff}>
            <LogoutOutlined />
          </IconButton>
        </div>
      </div>
    </header>
  );
}

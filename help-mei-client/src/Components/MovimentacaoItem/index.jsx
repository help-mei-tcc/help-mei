import "./style.css";
import { IconButton } from "@mui/material";
import {
  DeleteOutlineOutlined,
  ModeEditOutline,
} from "@mui/icons-material";

export function MovimentacaoItem({
  idMovimentacao,
  valor,
  descricao,
  dataInsercao,
  categoria,
  tipoMovimentacao,
  onEditar,
  onExcluir,
}) {
  return (
    <div className="movimentacao-card">
      <div
        className={`card-color ${
          tipoMovimentacao == "ENTRADA" ? "green" : "red"
        }`}
      ></div>

      <div className="card-bar"></div>

      <div className="card-descriptrion">{descricao}</div>

      <div className="card-bar"></div>

      <div className="card-category">{categoria}</div>

      <div className="card-bar"></div>

      <div className="card-date">{dataInsercao}</div>

      <div className="card-bar"></div>

      <div
        className={`card-value ${
          tipoMovimentacao == "ENTRADA" ? "fontGreen" : "fontRed"
        }`}
      >
        R$ {valor}
      </div>

      <div className="card-bar"></div>

      <IconButton onClick={() => onEditar(idMovimentacao)} aria-label="editar">
        <ModeEditOutline />
      </IconButton>

      <IconButton
        onClick={() => onExcluir(idMovimentacao)}
        aria-label="excluir"
      >
        <DeleteOutlineOutlined />
      </IconButton>
    </div>
  );
}

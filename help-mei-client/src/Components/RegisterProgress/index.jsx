import "../RegisterProgress/style.css";

export function RegisterProgress({ stage }) {
  return (
    <div className="register-progress">
      <div className="register-progress-ball filled" />

      <div className="register-progress-bar" />

      <div
        className={`register-progress-ball ${
          stage >= 1 ? "filled" : undefined
        }`}
      />

      <div className="register-progress-bar" />

      <div
        className={`register-progress-ball ${
          stage >= 2 ? "filled" : undefined
        }`}
      />
    </div>
  );
}
